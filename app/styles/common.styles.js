import { StyleSheet, Platform } from 'react-native';
import { colors } from './colors';
import { fonts } from './fonts';
import RF from "react-native-responsive-fontsize";

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
  button_static: {
    backgroundColor: colors.themeColor,
    height: RF(fonts.btn_height),
    fontSize: RF(fonts.btn_text),
    textAlign: 'center',
    alignSelf: 'center',
    width:RF(fonts.btn_width),
    color: colors.white,
    borderRadius: RF(fonts.btn_radius),
    padding: RF(fonts.btn_padding),
    ...Platform.select({
        ios: {
          fontFamily: 'Ubuntu-Medium',
        },
        android: {
          fontFamily: 'UbuntuMono-RI',
        }
      }),

  },
  button_dynamic: {
    backgroundColor: colors.themeColor,
    textAlign: 'center',
    alignSelf: 'center',
    color: colors.white,
    borderRadius: RF(fonts.btn_radius),
    padding: RF(fonts.btn_padding),
    ...Platform.select({
        ios: {
          fontFamily: 'Ubuntu-Medium',
        },
        android: {
          fontFamily: 'UbuntuMono-RI',
        }
      }),
  },
});