

export const fonts = {
    headerfont: 3.5,
    drawertextfont: 2.5,
    buttonfont: 2.0,

    avatar_view_height: 18.0,
    avatar_height: 14.0,
    avatar_width: 14.0,
    avatar_radius: 7.0,

    drawer_button_font: 2.4,
    btn_text: 2.8,
    btn_height: 7.5,
    btn_width: 40.5,
    btn_radius: 1.5,
    btn_padding: 1.5,

}