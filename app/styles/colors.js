

export const colors = {
    statusbarcolor: '#404040',
    themeColor: '#404040',   // primarycolor1
    primarycolor: '#14B4C2', // primarycolor2
    Secondary1: '#08D500',
    Secondary2: '#F9AC19',
    Secondary3: '#E22F39',

    bgcolor: '#FFFFFF',
    white: '#FFFFFF',
    dimwhite: '#F0F0F0',
    disable: '#666666',
    black: '#000000',
    gray:'#808080',
    darkgray:'#474444',
    lightgray:'#D3D3D3',
    green:'#006400',
    greenlight: '#4ed965',
    bgr:'#393939',
    hometext: '#7C5353',
    blue: '#91CAFA',
    lightblue: '#0C8EFA',
    dashfeecolor: '#04ECF0',
    yellow: '#FCF403',
}