import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar
} from 'react-native';
import { colors } from '../styles/colors';
import commonstyles from '../styles/common.styles';

class Next extends Component {

    static navigationOptions = {
        title: 'Next',
        headerStyle: {
          backgroundColor: colors.themeColor,
        },
        headerTintColor: colors.white,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      };

    async componentWillMount() {
        StatusBar.setBackgroundColor(colors.statusbarcolor);
      }    
   
    render(){
        console.disableYellowBox = true; 
        return (
            <View style={commonstyles.container}>
                <Text>Next Screen</Text>
                </View>
        );
    }
}

export default Next;
