import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    Alert
} from 'react-native';
import {Header, Left,Right,Icon} from 'native-base';
import { fonts } from '../styles/fonts';
import { colors } from '../styles/colors';
import RF from "react-native-responsive-fontsize";
import AppHeader from '../components/AppHeader';
import commonstyles from '../styles/common.styles';

class HomeScreen extends Component {

    static navigationOptions = {
        header: null
    }

    async componentWillMount() {
        StatusBar.setBackgroundColor(colors.statusbarcolor);
      }

    // static navigationOptions = {
    //     drawerIcon: ({ tintColor }) => (
    //         <Icon name="home" style={{ fontSize: RF(fonts.drawer_button_font), color: tintColor }} />
    //     )
    // }

    _appDrawer = () => {
        this.props.navigation.openDrawer();
    }

    _loadWelcome = () => {
        this.props.navigation.navigate('Welcome', {
            itemId: 86,
            otherParam: 'anything you want here',
          });
    }

    render(){
        console.disableYellowBox = true; 
        return (
            <View style={styles.container}>
             <AppHeader _appDrawer={this._appDrawer}/>
            <View style={commonstyles.container}>
                <Text>Home Screen</Text>
                <TouchableOpacity onPress={this._loadWelcome} >
                   <Text style={commonstyles.button_static}>Go Next</Text>
              </ TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

export default HomeScreen;
