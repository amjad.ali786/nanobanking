import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import {Header, Left,Right,Icon} from 'native-base';
import { fonts } from '../styles/fonts';
import { colors } from '../styles/colors';
import RF from "react-native-responsive-fontsize"
import AppHeader from '../components/AppHeader';
import commonstyles from '../styles/common.styles';

class SettingsScreen extends Component {

    async componentWillMount() {
        StatusBar.setBackgroundColor(colors.statusbarcolor);
      }

    // static navigationOptions = {
    //     drawerIcon: ({ tintColor }) => (
    //         <Icon name="settings" style={{ fontSize: RF(fonts.drawer_button_font), color: tintColor,marginLeft:5 }} />
    //     )
    // }

    _appDrawer = () => {
        this.props.navigation.openDrawer();
    }

    _loadWelcome = () => {
        this.props.navigation.navigate('Welcome2');
        //alert("hello2");
    }

    render(){
        return (
            <View style={styles.container}>
            <AppHeader _appDrawer={this._appDrawer}/>
            <View style={{flex: 1, alignItems:'center', justifyContent:'center'}}>
                <Text>Settings Screen</Text>
                <TouchableOpacity onPress={this._loadWelcome} >
                   <Text style={commonstyles.button_static}>Go Next</Text>
              </ TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})

export default SettingsScreen;
