import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp,  listenOrientationChange as loc,
    removeOrientationListener as rol} from 'react-native-responsive-screen';
  import RF from "react-native-responsive-fontsize";
  import commonstyle from '../styles/common.styles';

class Login extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
    }

    
    render(){
        return (
            <View style={commonstyle.container}>
                <TouchableOpacity onPress={ () => this.props.navigation.navigate('Config') }>
                   <Text style={commonstyle.button_static}>Click Me</Text>
              </ TouchableOpacity>
            </View>
        );
    }
}

export default Login;
