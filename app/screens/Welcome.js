import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar,
    TouchableOpacity
} from 'react-native';
import { colors } from '../styles/colors';
import commonstyles from '../styles/common.styles';

class Welcome extends Component {

    static navigationOptions = {
        title: 'Welcome1',
        headerStyle: {
            backgroundColor: colors.themeColor,
        },
        headerTintColor: colors.white,
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    async componentDidMount() {
        this.props.navigation.addListener("didFocus", () => {
            console.log('user has navigated to this screen');// user has navigated to this screen
        });
    
        this.props.navigation.addListener("didBlur", () => {
            this._backFunc();
            console.log('user has navigated away from this screen');// user has navigated away from this screen
        });
    }

    async componentWillMount() {
        StatusBar.setBackgroundColor(colors.statusbarcolor);
        //Get the param, provide a fallback value if not available
        const { navigation } = this.props;
        const itemId = navigation.getParam('itemId', 'NO-ID');
        const otherParam = navigation.getParam('otherParam', 'some default value');
        console.log('ItemID === ', itemId);
        console.log('otherParam === ', otherParam);
    }

    _backFunc() {
        console.log('=======_backFunc');
    }
    _loadNext = () => {
        this.props.navigation.navigate('Next');
    }

    render() {
        console.disableYellowBox = true;
        return (
            <View style={commonstyles.container}>
                <Text>Welcome Screen</Text>
                <TouchableOpacity onPress={this._loadNext} >
                    <Text style={commonstyles.button_static}>Go Next</Text>
                </ TouchableOpacity>
            </View>
        );
    }
}

export default Welcome;
