import React, { Component } from 'react';
import {
    View,
    Text,
    StatusBar
} from 'react-native';
import { colors } from '../styles/colors';
import commonstyles from '../styles/common.styles';

class Welcome2 extends Component {

    static navigationOptions = {
        title: 'Welcome2',
        headerStyle: {
          backgroundColor: colors.themeColor,
        },
        headerTintColor: colors.white,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      };

    async componentWillMount() {
        StatusBar.setBackgroundColor(colors.statusbarcolor);
      }    
   
    render(){
        console.disableYellowBox = true; 
        return (
            <View style={commonstyles.container}>
                <Text>Welcome2 Screen</Text>
                </View>
        );
    }
}

export default Welcome2;
