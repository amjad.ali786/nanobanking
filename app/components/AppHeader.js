import React, { Component } from 'react';
import { Text, View, Platform, Image } from 'react-native';
import { Header, Left, Right, Icon, Form } from 'native-base';
import { colors } from '../styles/colors';


class AppHeader extends Component {

    _appDrawer = () => {
        this.props._appDrawer();
    }
    render() {
        return (
            <Header style={{ backgroundColor: colors.themeColor }}>
                <Left style={{ marginLeft: 5 }}>
                    <Icon name="menu" style={{ color: colors.white }} onPress={this._appDrawer} />
                </Left>
                <Right />
            </Header>
        );
    }
}

export default AppHeader;
