import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View, ScrollView,
  StatusBar, Image, AsyncStorage,
  BackHandler, TouchableOpacity
} from 'react-native';
import { createDrawerNavigator, DrawerItems, createStackNavigator, SafeAreaView } from 'react-navigation';
import { Header, Left, Right, Icon } from 'native-base';
import HomeScreen from '../screens/HomeScreen';
import SettingScreen from '../screens/SettingsScreen';
import Welcome from '../screens/Welcome';
import Welcome2 from '../screens/Welcome2';
import Next from '../screens/Next';
import { imagepath } from '../utility/imagepath';
import { colors } from '../styles/colors'
import { fonts } from '../styles/fonts';
import RF from "react-native-responsive-fontsize"
import ImagePicker from 'react-native-image-picker';
import ImgToBase64 from 'react-native-image-base64';

class Config extends Component {

  constructor(props) {
    super(props);
    // this.state = {
    //   isAvatar: imagepath.user,
    // }
  }

  static navigationOptions = {
    header: null,
  }

  async componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }


  handleBackButtonClick = () => {
    return true;
  };

  render() {
    return (
      <AppDrawerNavigator />
    );
  }
}

class CustomDrawerComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isAvatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGoAAABkCAYAAABuK6XnAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAD/1JREFUeAHtnXlsVMcdx/eyMZe5k0JTwkYFDAbMYZuUoIIjJVKVRjR/QMsfqaJIUdpU6qGEqqpa5WhU1PzR44+2Kv+kJVVEIU2LorRJFSWgkoT7DJgc4gghAnHfGHt3+/lOdrZre9c4eN+bSfxG2p3d9+a9+c3vO79jfjPzXjz2GUurV69OiuQlS5ZklB85cmTgiRMnmtvb2+dnMpk5HR0d9blcbiy/B+t8Mpm8wGd/TU3NysbGxj/o2NatW6vmzJmT5Wc2Ho/ndMz3FPedQEsfzI+vW7cu2dLS0qFju3fvnn758uUHs9nsIpidrq6ujgFO7Nq1azHAilHeXMq5mM4NGjQodvXq1X+PGDHiocmTJx/tet+FCxfmKGvAt+d8yj8TQL3xxhspC1Bra+v08+fP/xxAFg8cODB25cqVGNJkpUMAJWC42mXbJsTMecCq4rqLVVVVfxkwYMALw4cP35lOp88WA1JcV/Fx179tY1zTUbZ+qSlUVjsApLZs2bK8ra3tMdRY7MKFCzHwaOcjVRiXxJW9yf9PdFA+NWTIEIErCTzGdR8A2lY+LzU0NLyuolKv+/btyz3xxBMC2IvUm8Y5I9T2bqm5S5curYKZU8+ePWsBSvUSnE70AxTaMisAEqlUKiG1SG7UJYe3A+JTM2bMWKuLBJi1hZ1u4uCPt0BZkJCiRUjRC4lEIoWNuUZedSMAleJtHjSpRm6ZS6BKDXBI2vMjR458cOLEiW0cT1LOue3yEqgikO6nl6+UHSJJ/VWVYngFj2WEGLYrhS3bNmrUqDsB6zyHZPecqsFEBRtZkVtZkDZv3nyvQELlqbuLgUGDJPolPSnUaxvqcM7Jkydf1kGBRP1OO7VXQMEM435jk+ro0S9IkvJqx4ydxLQwEnUOwLNsw2mZv2nTpqdV5zqGBmHUXa4Op72kmCj1WJIZ/Lz55ptbGKQ2AlQ7ZcKQpGJS7G/REkeyrg0bNmwKDsYBaHSmAr2RKNtjN27c+ChGvZHBrEuQBJY6cTtjr2rU71IdWLFihTOp8kKirDQdPnx4xEcffbQfr+sm7JPsguuO1AFQKeh5Zd68eV/r9xJlpen48eP3YxdugjHy8FyDZKIcCkdBTxp6UqhmZ06Fc2ZIxRAeMuMUogVLcSKkZXygK8aYzQyEsVPDjh49OkyEPfnkk060kJNK1WCbrDrZuXPnJDytd+i9ch6MIbdlHObGwyF6cZ4B8JS6urqPLb1h0+S856L2DA1IUjNOhKIOio4770DFQKD+qk+dOuXK+zSkOAfKMgSAZmEDNG4yLro97kOOXyNvL+WSFqeVq+FM+hlgsE+T84zwSproQOo8caTKaad2ChRMEA9swPOL2Cdh5RVQ+c4jkAyv1qxZ44Q+p70kzwS5wYOJRIyQKyzk7HGP8gSTjU47tVOgrKu7bdu2gai+WqkZ35JoohPFmAurdkmb094LE0zsbMOGDeMB6l2EqYZjvrjmxrHBkYjhjcaZUGyZPn36OshzMj/lVKJsD4UJNZoY/AQje9R9Dj2a4s9Johg+jHBJkVOgigxzlae2SdGJrIAikn+zgEJNO+GZk0q79kxG/sLJqRruSlPR/7jCWpC3RMe00IYsdFq9AAr7VMQX734mkaYMweIWpmAWiTpmoUOf7vACKFxfuejeIWQJQppycirw/CbYY2HnXgAFSFo44i9SeVQY55lVNmGDpPqcDuIWL15swEH1dci7csGA3tQJaXFFTQDqSG/KB1HGC4liqrsN1ZLx1J9QnCuJQ3G1trb2XYHAOvXQl445BYrIhOl8o0ePvoQLfFlAyR4E0SP7cM8stknRiVYtcNF9oLF/AfX4448bUCZNmnSRWdTTmlGV0fYsGaAA51XRpXWHLuhzKlGSHpLCSFfJP9A6cBe9tRzjRR/nUlpfOHjw4DUqZ6dlyl0T1HGnQKlRdoYX1fI3pEqHvFF9dJ4Mq5DiSPpr7PTYzu6OhKtNA86BwjCbSajbb7/9WTYBbNB0PGCZzWpCzWUCKBOBoBMtFx0LFixwxi9nFVsApF7sdk+M9kO46m2ck2i5lqx2VsgmAWvl3LlzXxeNdjOdpT3M3DlQaqzUSX7D2n7A+s3QoUN12KVUZYiWVGGbDgLSIyJGG9uUu0pOPJhSjT1w4IBx99jyspJdFD+hJyuiLuaEGgBVnXie2uQml/xh/l+yux5L0R3WsVCZcL1GAY6ZlGPLzc8w4L9gnZ8GwWEHQM0yZqIQa++4445v2G1A16M96PNeqD7bSEDJyrNqbm5+Glu1mglFgRSqCqSzmKl3NN8/LV0+5F4BBUNyDIKtp/UUXiB8M2u+w7IPJlykcRNpr75cjZtUd3HySvVZwgDHrKV466231qECF7AFJywVmGHQLU9vL2pvuuwVicx9WMs3iTJY2UEwDPpvyIPgrB6NAFirBI7skw8giSleAmXQ4guva0c+9heG5Et0qpDey3SO50SDiyi5bXvX3EugLINwJrbx6ILLqD85FUHbqQzxPD1z4qXZs2cf1gAX4LyJEHsJlBgk72/q1KmHkaqNhJXUwezS566drSL/ZYt0I9TeP5SPGTMmDClWVb1KXgIlym1cjR7+J4DTmoogGWe8PbzMNoDaqvqtVOu3DynIxvepfcXeFitpNyNZTTBSUlXxATB1ZQEowXT7ofHjx09Jp9MaF9A/3Ht7loneSpSYJK9LhALSv2CkfgZiM1QXdjCGx3dGIKkin0ASPd4CJeJsIkqwN0jvz6pVJKri0mrb0Nfca6CwE8bTQ5ouwUxN0wdCr6RJW34IW928Y8eO4WKqBa+vDK7U9YE0vBLEiVFvv/220XeEdKrFTNRRIKqPuhIApbURNwNWg+hnjbk3Mwuix0ugbESAh3BcgYk1MPG7MFBABeb8qBPo9tT1qBijNebWRuq/6xRYw2+0YWKOZlLPnDkz/L333lsG4x6AgePw+MTIQDsWnUKDXsX6lrM04Kdqg6XnRttTqeu8AsoyhWdO3M2izD8z0B178eJFPTklELe8KxPpCGaNuRa0YA/3ANoP9fhSS1fX8mH+9wIoRSE0wJUkMZv6CEz6PaEjGXc98VIb3AKVpBIM78BepRgWaH/U0qamplWuZ3mdAwUIilCbyUGejbeMSMQz586d02hTjoMzd5n6zRM3Fb4CtK/PmjXr5b1791bX19ebZwCVADfQQ048G8AxzzBfv3697I4F6df8/hFPnzQ7/CjjDCRxnPq1bC2DZCexky+ijmcDEli5ASs0oAQODU8w1wQeBhwDEOOWu3C/n0HNzNQaCVRNIl9W/HKdkqjhDmirZvbjlQ8//HAuIaaPBRafDKunJPVBR/UNDwJTfXlmS3KMfSleE8e5GgC6B4C+BxNaYIae8m+eSe4amTL1G5tFBzuCg3EPmwX22HKyXbw+QjPQgYzxbD0VA6onYFSZwNmzZ89ceuZ9qJJF6P0Jcubw7rISIoo4VXWWIT3kHYSy5Ni0Ybd+RVzwxWnTpu2y5a1DRDRFoFVcym4YKBGmhShSZVoAUmpNNg/xvQ3P7SvsLboLUL6K9Jh3aACWPDrboNDUr2VqH3IzTNACUS2AoYPtALwXWYv4d3aktNr7yp1X+AvAKjaH1mug6EmyL0aNleo177//vp5sfBvE6o0z8/nMRaXV0fv0kI+Y3G0Ak3qQBMkOhe1yWz72NZe06PHeKa2vQDPE6HgZOuF6QHuezW5reUnLSVtJHjQ5TX1SjT0CBTEF7wwJ6lQRxvQLMH8azG+kXBOgzEBq0gBjVJhCPpyTyjNOA7nw0bKeHuu0DfwM5ALMdDzaVMUg2bw6Alt7EsDWwofnsGXrbTs0ta9Z41Kd3JbpKS/LNDG0WNceOnRoLCquGTBa+BhpgaDhfBQ5MKAIHK6TU6A69SWpKVuHCn1OkpUyzZ2ltPYiz4vdRP6fZ+3Hap6eedC2VVJWzlzYMl3zkky0IJEn8c4eoJd8C4lpptJapMIQIWkBoIJIU/bzJjFdedWr/1Jx8MI8YRq1mJB6RDVepUO/Cv/+in37j14joZtJyrThXNdc7+YlgbKxLR6A8Vu2nvyAAKkBB2CsGypJ6S/Scj0e9nRezoRAMKpRnZwOfpToy2uoxmetauxNeKobUJIMIcw2k1t5juq73HgAFZj3NOlcT1RF58pywKhGnZVqBCRjLuDzGiRsmVZb5Xkr6VLZbqkb461nx4u0vskNB3ChnAE9lLdb2W53iw6U44AEQsOQFCYky4xAR378uPj06dP7t2/f/h0Jh0AidRMe3bQb84kgGN+f+90ng8jFJS/UxVH69BzId/iUDDqAweJ2PQLvj8xm/66nu3UCKn+T3K5duyZzg0acCF3bqUxPN4vOfWoOaLNeFqnSO0C+D1g/llTJR+h6p04gWLUHQAu5UOESqb1IorpyrYL/JRykJP6AlgH8Et9gouKieaEp1NQJKLsXCLV3Jx+pvULB6EdwHBAo8FpSlSS686BqskJjay2IGIUpG88cPHiwhjfONGkQm7+BLRvlAXJAvM6PTeerGkUwiqsrSBSPDTW/0ZeTuGiCLhJyxYWj34FywDzBjDHWSPhv92UV+F8Ayu5ewIlowC0XRpF9ChSXzjcXw2Vu4P8Y7FStzgJYoVABKHuEwuYdGSpnj0V5KBzQayW0NXUMbvs81YidKszRFYDS+oU8OTO4wNinUMiLKilwAKGSoyDef1sHrXOn30YHckKSl2NOqfbYsWOt/B+HZPnw6jrR2J8SrM/FkaosoSWWFDa8o8CtJmWNRFlHApGbDGDj8Pik9gqGrD9xynFbJTDt+AgJIu4PixbrOxj33P7B07tbYXlyuYYF190x8f2qeiRKj+9Wm+/h92MAp9egf7KW28b3kKR78Toit9xh1zAi1d4uW5RmmmmmSJHGS2iRCr9ln24BqHqtbSAVnAz9iVKoHFAIPaO1GISWmlWzNJ5Z860/LCNuwIgNwYeQ2ovsk5jiMAGW5qzmWBIKksPBJia1pPasm27LRHnIHMirP9Vary8FaRPElAwwjJ0aAUvHI2kSF9wm7YBUNH1Ca2vrKJFipt0ViEXlTZUjgcgVpMwtrf23diTK7CkmH816lfEGKH0RiE0D0Jcij8+PzgEW0mrGoWD10m2iykgP0lTP+EkThZEj4QdWosK8CQ5zNFF/LFAzI0dC7PAnSf0hQPp0AsoEYiEzciQ8wUrqL+/cTRBJWqxfjXdRFzkSniCUJ0MSJaD43AJGVXFWHKUZ7O5DxLx6tapfbHNCjVa9xDFJ5/AfpkiiJhKRkHuuBYBOKIoqLckBo/qAZRjabmyKpWFTtSODpPmnKGJekmfODuolmEnifrdowFsHQM4oiSouzwGwMU8IwDTdmpBEKVxBivReeZ45OYMAKZKud1eNl6qboqkN0ItCR07gKF+pfAZslCZy09pdYIJ+FI8kqjzPnJxBmkxwFoy+LCmKAHICw/UrRaLM688pmf4fUSA7NYRFwgIAAAAASUVORK5CYII=',
    }
  }

  async componentWillMount() {
    try {
      const avatar = await AsyncStorage.getItem('imgAvatar');
      if(avatar != '' && avatar != null){
       this.setState({isAvatar: 'data:image/png;base64,'+avatar});
      }
    } catch (error) {
      console.log(error + 'On retrieveItem');
    }
  }
  
  async storeItem(key, item) {
    try {
      var jsonOfItem = await AsyncStorage.setItem(key, item);
      return jsonOfItem;
    } catch (error) {
      console.log(error.message);
    }
  }

  _setAvatar = () => {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {

        console.log('User cancelled photo picker');
      }
      else if (response.error) {

        console.log('ImagePicker' + response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ' + response.customButton);
      }
      else {
        let source = response.uri;
        if (source != null) {
          ImgToBase64.getBase64String(response.uri).then(base64String => 
            this.storeItem('imgAvatar', base64String)).catch(err => doSomethingWith(err));
          this.setState({ isAvatar: source });
        } else {
          alert('try again !');
        }
      }
    });
  }

  render() {          
    return (   
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar
          backgroundColor={colors.statusbarcolor}
        />
        <View style={styles.avatar_container}>
          <TouchableOpacity onPress={this._setAvatar} >
            <Image source={{uri: this.state.isAvatar}} style={styles.avatar_style}></Image>
          </TouchableOpacity>
        </View>
        <ScrollView>
          <DrawerItems {...this.props} />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const AppStackNavigatorHome = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: null,
    },
  },
  Welcome: {
    screen: Welcome,
    navigationOptions: {
      //header: null
    }
  },
  Next: {
    screen: Next,
    navigationOptions: {
      //header: null
    }
  },
})

const AppStackNavigatorSettings = createStackNavigator({
  Setting: {
    screen: SettingScreen,
    navigationOptions: {
      header: null,

    }
  },
  Welcome2: {
    screen: Welcome2,
    navigationOptions: {
      //header: null
    }
  },
})

const AppDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: AppStackNavigatorHome,
    navigationOptions: {
      //drawerLabel: 'Home',
      drawerIcon: ({ tintColor }) => (
        <Icon name='home' style={{ fontSize: RF(fonts.drawer_button_font), color: tintColor, marginLeft: 5 }} />
      ),
    },
  },
  Setting: {
    screen: AppStackNavigatorSettings,
    navigationOptions: {
      //drawerLabel: 'Setting',
       drawerIcon: ({ tintColor }) => (
         <Icon name='settings' style={{ fontSize: RF(fonts.drawer_button_font), color: tintColor, marginLeft: 5 }} />
       ),
    },
  }
},
  {
    contentComponent: CustomDrawerComponent,
    //drawerWidth: Dimensions.get('window').width - 130
    contentOptions: {
      activeTintColor: colors.primarycolor
    }
  })

const styles = StyleSheet.create({
  avatar_container: {
    ...Platform.select({
      ios: {
        marginTop: 35,
      },
      android: {
        marginTop: 0,
      }
    }),
    height: RF(fonts.avatar_view_height),
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatar_style: {
    height: RF(fonts.avatar_height),
    width: RF(fonts.avatar_width),
    borderRadius: RF(fonts.avatar_radius)
  }
});

export default Config;