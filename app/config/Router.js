
import { StackNavigator } from "react-navigation";
import Config from './Config';  
import Login from '../screens/Login';

export const Router = StackNavigator({

    Login: {
        screen: Login,
        navigationOptions: {
        }
    },
    Config: {
        screen: Config
    }, 
});
