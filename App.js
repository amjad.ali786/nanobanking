

import React, {Component} from 'react';
import { Router } from './app/config/Router'

export default class App extends Component {
  render() {
    return (
     <Router />
    );
  }
}
